package com.balu.learn;

import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.balu.learn.model.Type;
import com.balu.learn.steps.Steps;

@ConfigurationProperties(prefix="pricing.engine")
public class AppProperties {

	public Map<Steps, List<Type>> steps;
	
	public List<Steps> stepOrdering;

	public Map<Steps, List<Type>> getSteps() {
		return steps;
	}

	public void setSteps(Map<Steps, List<Type>> steps) {
		this.steps = steps;
	}

	public List<Steps> getStepOrdering() {
		return stepOrdering;
	}

	public void setStepOrdering(List<Steps> stepOrdering) {
		this.stepOrdering = stepOrdering;
	}
	
	
}
