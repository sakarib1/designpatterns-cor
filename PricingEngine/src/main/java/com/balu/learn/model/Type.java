package com.balu.learn.model;

public enum Type {
	FACILITY_IN_PATIENT(100), 
	FACILITY_OUT_PATIENT(101),
	PROFESSIONAL(102),
	BLUECARD_HOME(103),
	PROFESSIONAL_RPT_35(104);

    private int _claimType;

    Type(int claimType) {
        this._claimType = claimType;
    }

    public int claimType() {
        return _claimType;
    }
    
    public static boolean contains(int value)
    {
        for(Type claimType: Type.values())
             if (claimType.claimType() == value) 
                return true;
        return false;
    } 
    


}
