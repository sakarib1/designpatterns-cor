package com.balu.learn.steps.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.balu.learn.AppProperties;
import com.balu.learn.model.Type;
import com.balu.learn.steps.AbstractStep;
import com.balu.learn.steps.Step;
import com.balu.learn.steps.Steps;


/*
 * Load configurations from properties file to Java memory nad create the objects
 */
@Configuration
public class StepConfiguration {

	public static final Map<Integer, Step> CONFIG = new HashMap<>();
	
	@Autowired
    private AppProperties appProperties;

	@Autowired
	private List<AbstractStep> stepBeans;
	
	@PostConstruct
	public  void loadConfiguration(){
		//load config from properties file and then create STEPS object map
		
		
		//step1ConfigList = step1ConfigList.stream().filter(line -> Type.contains(line)).collect(Collectors.toList());  
		
		loadStepClaimTypeConfigurations();

		firstStep();	
		
		
		//System.out.println(StepConfiguration.CONFIG);		
		
		
		//new Step(result, Steps.STEP1); 
		
		
	}

	@Bean
	public AbstractStep firstStep() {
		// TODO Auto-generated method stub
		System.out.println(appProperties.stepOrdering);
		System.out.println(stepBeans);
		AbstractStep currentStep=null;
		AbstractStep nextStep=null;
		AbstractStep firstStep =null;
		for(Steps step : appProperties.stepOrdering) {
			for(AbstractStep stepImpl : stepBeans) {
				if(stepImpl.stepId._stepId() == step._stepId()) {
					System.out.println("CreatingCOR-->"+stepImpl.stepId);
					nextStep =stepImpl;
					if(firstStep==null) {
						firstStep = nextStep;
					}else {
						currentStep.linkWith(nextStep);
					}
					currentStep = nextStep;
				}

			}
		}
		return firstStep;
	}


	public void loadStepClaimTypeConfigurations(){
		System.out.println(appProperties.steps);

		Map<Steps,List<Type>> stepPropertiesMap= appProperties.steps;
		
		for (Map.Entry<Steps,List<Type>> entry : stepPropertiesMap.entrySet()) {
            Steps key=entry.getKey();
            List<Type> value=entry.getValue();
            
            switch (key) 
            { 
    		
    			case STEP1:{
    				System.out.println("Step1: "+key);
    				Step step = new Step(value,Steps.STEP1);
    				CONFIG.put(Steps.STEP1._stepId(),step);
    				break;
    			}
	            case STEP2:{
    				System.out.println("Step2: "+key);
    				Step step = new Step(value,Steps.STEP2);
    				CONFIG.put(Steps.STEP2._stepId(),step);
	            
	            	break;
	            }
	            case STEP3:{
    				System.out.println("Step3: "+key);
    				Step step = new Step(value,Steps.STEP3);
    				CONFIG.put(Steps.STEP3._stepId(),step);
	            	break;
	            }
	            default:
	            	break;
	            
            
            }
		}
		
		
	}

	
}
