package com.balu.learn;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.balu.learn.model.Claim;
import com.balu.learn.model.Type;
import com.balu.learn.steps.AbstractStep;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class PricingEngineApplication {

	@Autowired
	AbstractStep firstStep;
	
	public static void main(String[] args) {
		SpringApplication.run(PricingEngineApplication.class, args);
	}

	@PostConstruct
	public void simplyExecuteCoR() {
		System.out.println("CLAIM-1 :: Execution Start");
		firstStep.process(new Claim(Type.FACILITY_IN_PATIENT));
		System.out.println("CLAIM-1 :: Execution End***");

		System.out.println("CLAIM-2 :: Execution Start");
		firstStep.process(new Claim(Type.PROFESSIONAL_RPT_35));
		System.out.println("CLAIM-2 :: Execution End***");

		System.out.println("CLAIM-3 :: Execution Start");
		firstStep.process(new Claim(Type.BLUECARD_HOME));
		System.out.println("CLAIM-3 :: Execution End***");


	}
	
}
