### Chain Of Responsibility

is a __Behavioral Pattern__, that addresses responsibilities of objects in an application and how they communicate between them.

*Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it.*

* Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it.
* Launch-and-leave requests with a single processing pipeline that contains many possible handlers.
* An object-oriented linked list with recursive traversal.

Chain of Responsibility simplifies object interconnections. Instead of senders and receivers maintaining references to all candidate receivers, each sender keeps a single reference to the head of the chain, and each receiver keeps a single reference to its immediate successor in the chain.

#### Requirements
* Need to efficiently process the requests without hard-wiring handler relationships and precedence, or request-to-handler mappings.

#### Assumptions
* Assumes that all the object handlers in the chain handle the same input object and enrich it.
* Assumes that all the object handlers execute in a sequential way. 
* The order of handlers is constant.

#### Benefits: 
* Decoupling
* Changing the handlers list will not affect the requester's code
* Multiple handlers may be able to handle a request
* The requester knows only a reference to one handler

#### Constraints
* Constraints all object handlers in the chain to handle the same input object and enrich it.
* The requester doesn't have any control over the handlers

#### Variation
* The requester doesn't know how many handlers are able to handle its request
* The requester doesn't know which handler handled its request
* The handlers could be specified dynamically


Non-classic CoR 1: Send request through the chain until one node wants to stop
Non-classic CoR 2: Regardless of request handling, send request to all handlers

#### Solution
The solution is to decouple the chain execution decision-making and the request-handling by moving the next node call to the base class. Let the base class make the decision, and let subclasses handle the request only. By steering clear of chain execution decision-making, subclasses can completely focus on their own business, thus avoiding stopping the chain by accident.


#### Check list
- The base class maintains a "next" pointer.
- Each derived class implements its contribution for handling the request.
- If the request needs to be "passed on", then the derived class "calls back" to the base class, which delegates to the "next" pointer.
- The client (or some third party) creates and links the chain (which may include a link from the last node to the root node).
- The client "launches and leaves" each request with the root of the chain.
- Recursive delegation produces the illusion of magic.