package com.balu.learn.steps;

import com.balu.learn.model.Claim;
import com.balu.learn.steps.config.StepConfiguration;

public abstract class AbstractStep {
	
    private AbstractStep next;
    
	public Steps stepId;


    /**
     * Builds chains of pricing step objects.
     */
    public AbstractStep linkWith(AbstractStep next) {
        this.next = next;
        return next;
    }

    public boolean process(Claim claim) {
    	//Decide whether to process or execute next step
    	if(_isApplicable(claim)) {
    		this.preExecute(claim);
    		this.execute(claim);
    		this.postExecute(claim);
    	}
    		
        return processNextStep(claim);
 
    }
    
    /**
     * Runs check on the next object in chain or ends traversing if we're in
     * last object in chain.
     */
    private boolean processNextStep(Claim claim) {
        if (next == null) {
            return true;
        }
        return next.process(claim);
    }
    
    private boolean _isApplicable(Claim claim) {
 		return StepConfiguration.CONFIG.get(this.stepId._stepId()).isApplicable(claim.getClaimType());
	}
    
	/**
     * Subclasses will implement this method with pre processing step.
     */
    public abstract boolean preExecute(Claim claim);

	/**
     * Subclasses will implement this method with concrete processing step.
     */
    public abstract boolean execute(Claim claim);

	/**
     * Subclasses will implement this method with pre processing step.
     */
    public abstract boolean postExecute(Claim claim);
    

}