package com.balu.learn.model;

public class Claim {

	
	private Type claimType;
	
	
	

	public Claim(Type claimType) {
		super();
		this.claimType = claimType;
	}

	public Type getClaimType() {
		return claimType;
	}

	public void setClaimType(Type claimType) {
		this.claimType = claimType;
	}
	

	
	
}
