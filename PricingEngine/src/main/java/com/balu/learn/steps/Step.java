package com.balu.learn.steps;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.balu.learn.model.Type;

public class Step {
	private Map<Type,Type> applicableClaimTypes;
	private Steps id;
	
	public Step(List<Type> applicableClaimTypes, Steps id) {
		super();
		this.applicableClaimTypes = applicableClaimTypes.stream().collect(Collectors.toMap(t->t, t->t));
		this.id = id;
	}


	public Steps getId() {
		return id;
	}

	public void setId(Steps id) {
		this.id = id;
	}
	
	boolean isApplicable(Type type) {
		return applicableClaimTypes.containsKey(type);
	}


	@Override
	public String toString() {
		return "Step [applicableClaimTypes=" + applicableClaimTypes + ", id=" + id + "]";
	}
	
	
}
