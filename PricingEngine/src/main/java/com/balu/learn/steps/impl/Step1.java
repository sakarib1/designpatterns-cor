package com.balu.learn.steps.impl;

import org.springframework.stereotype.Service;

import com.balu.learn.model.Claim;
import com.balu.learn.steps.AbstractStep;
import com.balu.learn.steps.Steps;

@Service
public class Step1 extends AbstractStep {
	
	public Step1(){
		this.stepId = Steps.STEP1;
	}
	
	@Override
    public boolean preExecute(Claim claim) {
        
    	//TODO: execute any business logic on claim
    	
    	//return false; on any errors if
                
        return true;
    }
	
	@Override
    public boolean execute(Claim claim) {
        
    	//TODO: execute any business logic on claim
    	
    	//return false; on any errors if
        System.out.println("Step Execution: "+this.stepId);
        return true;
    }
    
	@Override
    public boolean postExecute(Claim claim) {
        
    	//TODO: execute any business logic on claim
    	
    	//return false; on any errors if
                
        return true;
    }
    
}