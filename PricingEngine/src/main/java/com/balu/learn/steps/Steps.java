package com.balu.learn.steps;

public enum Steps {
	STEP1(1), 
	STEP2(2),
	STEP3(3);

    private int _stepId;

    Steps(int stepId) {
        this._stepId = stepId;
    }

    public int _stepId() {
        return _stepId;
    }
	
}
